﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BlackJackIsh
{
    class Program
    {
        static void Main(string[] args)
        {
            var deck = new Dictionary<string, int>()
            {
                {"Ace of Clubs", 11},
                {"Ace of Diamonds", 11},
                {"Ace of Hearts", 11},
                {"Ace of Spades", 11},
                {"Eight of Clubs", 8},
                {"Eight of Diamonds", 8},
                {"Eight of Hearts", 8},
                {"Eight of Spades", 8},
                {"Five of Clubs", 5},
                {"Five of Diamonds", 5},
                {"Five of Hearts", 5},
                {"Five of Spades", 5},
                {"Four of Clubs", 4},
                {"Four of Diamonds", 4},
                {"Four of Hearts", 4},
                {"Four of Spades", 4},
                {"Jack of Clubs", 10},
                {"Jack of Diamonds", 10},
                {"Jack of Hearts", 10},
                {"Jack of Spades", 10},
                {"King of Clubs", 10},
                {"King of Diamonds", 10},
                {"King of Hearts", 10},
                {"King of Spades", 10},
                {"Nine of Clubs", 9},
                {"Nine of Diamonds", 9},
                {"Nine of Hearts", 9},
                {"Nine of Spades", 9},
                {"Queen of Clubs", 10},
                {"Queen of Diamonds", 10},
                {"Queen of Hearts", 10},
                {"Queen of Spades", 10},
                {"Seven of Clubs", 7},
                {"Seven of Diamonds", 7},
                {"Seven of Hearts", 7},
                {"Seven of Spades", 7},
                {"Six of Clubs", 6},
                {"Six of Diamonds", 6},
                {"Six of Hearts", 6},
                {"Six of Spades", 6},
                {"Ten of Clubs", 10},
                {"Ten of Diamonds", 10},
                {"Ten of Hearts", 10},
                {"Ten of Spades", 10},
                {"Three of Clubs", 3},
                {"Three of Diamonds", 3},
                {"Three of Hearts", 3},
                {"Three of Spades", 3},
                {"Two of Clubs", 2},
                {"Two of Diamonds", 2},
                {"Two of Hearts", 2},
                {"Two of Spades", 2}
            };

            playGame(deck.Clone());
        }

        static void playGame(Dictionary<string, int> deck)
        {
            var player = new Dictionary<string, int>();
            var bank = new Dictionary<string, int>();
            Console.WriteLine("Welcome to BlackJack!");
            GameDelay("Shuffling Deck");
            deck.Shuffle();
            Console.WriteLine("Shuffled. Ready? [Y]");

            var ready = false;
            while (!ready)
            {
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Y)
                    ready = true;
            }

            Console.WriteLine("\r              ");
            GameDelay("Drawing first card");
            player.Add(deck.Draw());
            Console.WriteLine("You got: " + player.Last().Key + $" ({player.Last().Value})");

            while (true)
            {
                var responded = false;
                var response = false;

                var p21 = player.CCount() == 21;
                if (!p21)
                {
                    Console.WriteLine($"You are at {player.CCount()}. Do you want to draw again? [Y/N]");
                    while (!responded)
                    {
                        var key = Console.ReadKey();
                        switch (key.Key)
                        {
                            case ConsoleKey.Y:
                                response = true;
                                responded = true;
                                break;
                            case ConsoleKey.N:
                                responded = true;
                                break;
                        }
                    }
                    Console.WriteLine("\r              ");
                }

                if (response)
                {
                    GameDelay("Drawing a card");
                    player.Add(deck.Draw());
                    Console.WriteLine("You got: " + player.Last().Key + $" ({player.Last().Value})");
                }

                else
                {
                    GameDelay("Banker is Drawing");
                    
                    while (bank.CCount() < 16)
                    {
                        bank.Add(deck.Draw());
                        Console.WriteLine("Banker got: " + bank.Last().Key + $" ({bank.Last().Value})");
                    }
                    
                    if (bank.CCount() > 21)
                    {
                        Console.WriteLine($"You win! (Banker had {bank.CCount()})");
                        return;
                    }
                    if (player.CCount() == bank.CCount())
                    {
                        Console.WriteLine($"Draw! Player and Banker {player.CCount()}");
                        return;
                    }

                    if (bank.CCount() > player.CCount())
                    {
                        Console.WriteLine($"You lose! (Banker had {bank.CCount()}; you had {player.CCount()}...)");
                        return;
                    }

                    if (bank.CCount() < player.CCount())
                    {
                        Console.WriteLine($"You win! (You had {player.CCount()}; Banker had {bank.CCount()})");
                        return;
                    }
                }

                if (player.CCount() > 21)
                {
                    Console.WriteLine();
                    Console.WriteLine($"You lose! (Player had {player.CCount()})");
                    return;
                }
            }
        }

        public static void GameDelay(string text)
        {
            Console.Write(text + ".");
            Thread.Sleep(500);
            Console.Write(".");
            Thread.Sleep(500);
            Console.WriteLine(".");
            Thread.Sleep(500);
        }
    }

    public static class DictionaryExtensions
    {
        public static void Shuffle<TKey, TValue>(this Dictionary<TKey, TValue> source)
        {
            Random r = new Random();
            var tmp = source.OrderBy(x => r.Next())
                .ToDictionary(item => item.Key, item => item.Value);
            source.Clear();
            foreach (var entry in tmp)
            {
                source.Add(entry.Key, entry.Value);
            }
        }

        public static KeyValuePair<string, int> Draw(this Dictionary<string, int> deck)
        {
            try
            {
                var card = deck.First();
                deck.Remove(card.Key);
                return card;
            }
            catch
            {
                return new KeyValuePair<string, int>("No cards in deck / deck is null", 0);
            }
        }

        public static Dictionary<TKey, TValue> Clone<TKey, TValue>(this Dictionary<TKey, TValue> source)
        {
            return source.ToDictionary(x => x.Key, x => x.Value);
        }

        public static int CCount<TKey>(this Dictionary<TKey, int> source)
        {
            var i = 0;
            foreach (var entry in source)
            {
                i += entry.Value;
            }

            return i;
        }

        public static void Add(this Dictionary<string, int> source, KeyValuePair<string, int> inp)
        {
            source.Add(inp.Key, inp.Value);
        }
    }

}